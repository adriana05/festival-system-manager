package festival.service;


import festival.domain.Show;
import festival.domain.Ticket;
import festival.domain.User;
import festival.domain.exceptions.LogInException;
import festival.domain.observers.IObserver;
import festival.domain.validator.Validator;
import festival.persistence.Configuration;
import festival.persistence.ShowRepository;
import festival.persistence.TicketRepository;
import festival.persistence.UserRepository;

import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Service implements IService {

    private final Map<String, IObserver> signedInClients;
    private final ShowRepository showRepository;
    private final Validator<Show> showValidator;
    private final TicketRepository ticketRepository;
    private final Validator<Ticket> ticketValidator;
    private final UserRepository userRepository;
    private final Validator<User> userValidator;


    public Service(ShowRepository showRepository, Validator<Show> showValidator, TicketRepository ticketRepository, Validator<Ticket> ticketValidator, UserRepository userRepository, Validator<User> userValidator) {
        this.showRepository = showRepository;
        this.showValidator = showValidator;
        this.ticketRepository = ticketRepository;
        this.ticketValidator = ticketValidator;
        this.userRepository = userRepository;
        this.userValidator = userValidator;
        signedInClients = new ConcurrentHashMap<>();
    }

    private void updateSeats(Show show, int seatsBought) {

        show.setNumberTicketsSold(show.getNumberTicketsSold() + seatsBought);
        show.setNumberTicketsAvailable(show.getNumberTicketsAvailable() - seatsBought);

        showRepository.update(show);

    }

    public synchronized Ticket saveTicket(Ticket ticket) {
        ticketValidator.validate(ticket);
        Ticket savedTicket = ticketRepository.save(ticket);
        updateSeats(ticket.getShow(), ticket.getNumberTicketsBought());

        notifyTicketSold(ticket.getShow().getID(), ticket.getNumberTicketsBought());

        return savedTicket;

    }

    private void notifyTicketSold(Long showId, Integer seatsCount) {

        Configuration.logger.traceEntry();

        int defaultThreadsCount = 5;
        ExecutorService executor = Executors.newFixedThreadPool(defaultThreadsCount);
        for (IObserver client : signedInClients.values()) {
            executor.execute(() -> client.ticketSold(showId, seatsCount));
        }
        executor.shutdown();
        Configuration.logger.trace("notified clients of seats sold");

        Configuration.logger.traceExit();

    }

    public synchronized User login(String email, String password, IObserver client) throws Exception {

        if (signedInClients.containsKey(email))
            throw new LogInException("user already signed in");

        User loggedIn = userRepository.filterByEmailPassword(email, password);
        signedInClients.put(loggedIn.getEmail(), client);
        Configuration.logger.trace("user {} signed in", loggedIn);

        Configuration.logger.traceExit(loggedIn);

        return loggedIn;

    }

    public synchronized Iterable<Show> findAllShows() {

        return showRepository.findAll();

    }

    public synchronized Iterable<Show> findShowsOnSpecificDate(LocalDate date) {

        return showRepository.filterByDate(date.atStartOfDay());
    }

    public void logout(String email, IObserver client) throws Exception {
        Configuration.logger.traceEntry("configuring LogOut with {} {}", email, client);

        if (!signedInClients.containsKey(email))
            throw new LogInException("user already signed out");
        signedInClients.remove(email);
        Configuration.logger.trace("user {} signed out", email);

        Configuration.logger.traceExit();
    }


}
