package festival.service;

import festival.domain.Show;
import festival.domain.Ticket;
import festival.domain.User;
import festival.domain.observers.IObserver;


import java.time.LocalDate;

public interface IService {

    Ticket saveTicket(Ticket ticket) throws Exception;

    User login(String email, String password, IObserver client) throws Exception;

    Iterable<Show> findAllShows() throws Exception;

    Iterable<Show> findShowsOnSpecificDate(LocalDate date) throws Exception;

    void logout(String toEmail, IObserver client) throws Exception;
}
