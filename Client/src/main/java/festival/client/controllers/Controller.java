package festival.client.controllers;

import festival.client.Configuration;
import festival.domain.User;
import festival.service.IService;
import javafx.stage.Stage;

public abstract class Controller {
    protected IService service = null;
    protected User signedInUser = null;
    protected Stage stage;

    public void init(IService services, User signedInUser, Stage stage) {
        Configuration.logger.traceEntry("Init controller with {} and {}", services, signedInUser);

        this.service = services;
        this.signedInUser = signedInUser;
        this.stage = stage;

        Configuration.logger.traceExit();

    }
}
