package festival.client.controllers;

import festival.domain.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController extends Controller {

    public PasswordField passwordTxtField;
    public TextField emailTxtField;
    public Label labelLoginError;

    public void handleLogin(ActionEvent actionEvent) throws IOException {
        User loggedInUser;
        String email = emailTxtField.getText();
        String password = passwordTxtField.getText();

        Stage mainStage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/gui/application.fxml"));
        AnchorPane root = loader.load();

        MainController mainController = loader.getController();

        mainStage.setScene(new Scene(root));
        mainStage.setTitle("Welcome");

        try {
            loggedInUser = service.login(email, password, mainController);
            mainController.init(service, loggedInUser, mainStage);
        } catch (Exception ex) {
            labelLoginError.setText(ex.getMessage());
            return;
        }

        stage.close();

        mainStage.setOnCloseRequest(event -> {
            try {
                mainController.handleLogOut(null);
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.exit(0);
        });

        mainStage.show();

    }


}
