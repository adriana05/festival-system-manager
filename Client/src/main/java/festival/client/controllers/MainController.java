package festival.client.controllers;

import festival.client.Configuration;
import festival.domain.Show;
import festival.domain.Ticket;
import festival.domain.User;
import festival.domain.exceptions.ValidationException;
import festival.domain.observers.IObserver;
import festival.service.IService;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MainController extends Controller implements IObserver {


    public TableView<Show> tableViewShows;
    public TableColumn<Show, String> showTableColumnArtist;
    public TableColumn<Show, String> showTableColumnLocation;
    public TableColumn<Show, String> showTableColumnDateTime;
    public TableColumn<Show, Integer> showTableColumnAvailableSeats;
    public TableColumn<Show, Integer> showTableColumnSoldSeats;

    public TableView<Show> tableFilteredByDateShows;
    public TableColumn<Show, String> filteredShowTableArtistColumn;
    public TableColumn<Show, String> filteredShowTableLocationColumn;
    public TableColumn<Show, String> filteredShowTableTimeColumn;
    public TableColumn<Show, Integer> filteredShowTableAvailableSeatsColumn;

    public Label labelFilterByDateShowsError;
    public Label labelSellTicketError;

    public DatePicker datePicker;
    public TextField seatsTxtField;
    public TextField textFieldArtist;
    public TextField textFieldDateTime;
    public TextField customerNameTxtField;
    public TextField textFieldLocation;
    ObservableList<Show> modelShows = FXCollections.observableArrayList();
    ObservableList<Show> modelShowsByDate = FXCollections.observableArrayList();

    private Show clickedShow = null;
    private final Long clickedShowId = (long) 0;

    private void setRowColorTableShows(TableView<Show> table) {

        table.setRowFactory(tv -> new TableRow<>() {
            @Override
            protected void updateItem(Show item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setStyle("");
                } else if (item.getNumberTicketsAvailable() <= 0) {
                    setStyle("-fx-background-color: #ff0000");
                } else {
                    setStyle("");
                }
            }
        });

    }

    @Override
    public void init(IService services, User signedInUser, Stage stage) {
        super.init(services, signedInUser, stage);
        try {
            setUpTableShows();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void initialize() {

        showTableColumnArtist.setCellValueFactory(new PropertyValueFactory<>("artist"));
        showTableColumnLocation.setCellValueFactory(new PropertyValueFactory<>("location"));
        showTableColumnDateTime.setCellValueFactory(new PropertyValueFactory<>("dateToString"));
        showTableColumnAvailableSeats.setCellValueFactory(new PropertyValueFactory<>("numberTicketsAvailable"));
        showTableColumnSoldSeats.setCellValueFactory(new PropertyValueFactory<>("numberTicketsSold"));
        tableViewShows.setItems(modelShows);

        setRowColorTableShows(tableViewShows);

        filteredShowTableArtistColumn.setCellValueFactory(new PropertyValueFactory<>("artist"));
        filteredShowTableLocationColumn.setCellValueFactory(new PropertyValueFactory<>("location"));
        filteredShowTableTimeColumn.setCellValueFactory(new PropertyValueFactory<>("timeToString"));
        filteredShowTableAvailableSeatsColumn.setCellValueFactory(new PropertyValueFactory<>("numberTicketsAvailable"));

        setRowColorTableShows(tableFilteredByDateShows);

    }

    @Override
    public void ticketSold(Long showId, Integer seatsCount) {
        Configuration.logger.traceEntry("Staring ticket sold with {} and {}", showId, seatsCount);

        Platform.runLater(() -> {
        updateShows(modelShows, showId, seatsCount);
        updateShows(modelShowsByDate, showId, seatsCount);
        });
        Configuration.logger.traceExit();
    }

    private void setUpTableShows() throws Exception {

        this.modelShows.setAll(StreamSupport.stream(service.findAllShows().spliterator(), false).collect(Collectors.toList()));

    }

    private void setUpFilteredTableShows() throws Exception {

        Show show = tableFilteredByDateShows.getSelectionModel().getSelectedItem();
        if (show == null)
            return;

        LocalDate date = show.getTime().toLocalDate();
        if (date == null)
            return;

        this.modelShowsByDate.setAll(StreamSupport.stream(service.findShowsOnSpecificDate(date).spliterator(), false).collect(Collectors.toList()));

    }

    private void updateShows(ObservableList<Show> model, Long showId, Integer seatsCount) {
        List<Show> shows = new ArrayList<>(model);

        for (Show show : shows) {
            if (show.getID().equals(showId)) {
                show.setNumberTicketsAvailable(show.getNumberTicketsAvailable() - seatsCount);
                show.setNumberTicketsSold(show.getNumberTicketsSold() + seatsCount);
                break;
            }
        }

        model.setAll(shows);
    }

    public void handleFilterByDate(ActionEvent actionEvent) throws Exception {

        LocalDate date = datePicker.getValue();

        if (date == null) {
            labelFilterByDateShowsError.setText("There is no date picked!");
            return;
        }

        labelFilterByDateShowsError.setText("");
        tableViewShows.setVisible(false);
        tableFilteredByDateShows.setVisible(true);

        modelShowsByDate.setAll(StreamSupport.stream(service.findShowsOnSpecificDate(date).spliterator(), false).collect(Collectors.toList()));
        tableFilteredByDateShows.setItems(modelShowsByDate);

    }

    public void handleShowAll(ActionEvent actionEvent) {

        tableFilteredByDateShows.setVisible(false);
        tableViewShows.setVisible(true);

    }

    public void handleLogOut(ActionEvent actionEvent) throws IOException {
        try {
            service.logout(signedInUser.getEmail(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        stage.close();

    }

    public void handleSelectionChangedShowsTable(MouseEvent mouseEvent) {

        Show show = tableViewShows.getSelectionModel().getSelectedItem();
        if (show == null)
            return;

        textFieldArtist.setText(show.getArtist());
        textFieldDateTime.setText(show.getDateToString());
        textFieldLocation.setText(show.getLocation());
        clickedShow = show;

    }

    public void handleSelectionChangedFilteredShowsTable(MouseEvent mouseEvent) {

        Show show = tableFilteredByDateShows.getSelectionModel().getSelectedItem();
        if (show == null)
            return;

        textFieldArtist.setText(show.getArtist());
        textFieldDateTime.setText(show.getDateToString());
        textFieldLocation.setText(show.getLocation());
        clickedShow = show;

    }

    private Ticket verifySellTicket() {

        if (clickedShow == null) {

            labelSellTicketError.setText("You must select a show!");
            return null;

        }

        String customer = customerNameTxtField.getText();
        String seatsCountString = seatsTxtField.getText();

        if (customer.equals("") || seatsCountString.equals("")) {

            labelSellTicketError.setText("Info tickets filled is not complete!");
            return null;

        }

        int seatsCount = Integer.parseInt(seatsCountString);

        if (seatsCount > clickedShow.getNumberTicketsAvailable()) {

            labelSellTicketError.setText("Not enough available seats!");
            return null;

        }

        return new Ticket(customer, seatsCount, clickedShow);

    }

    public void handleSellTicket(ActionEvent actionEvent) throws Exception {

        Ticket ticket = verifySellTicket();
        if (ticket == null)
            return;

        try {
            service.saveTicket(ticket);
        } catch (ValidationException ex) {
            labelSellTicketError.setText(ex.getMessage());
            return;
        }

        setUpTableShows();
        setUpFilteredTableShows();

        labelSellTicketError.setText("");

    }




}
