package festival.domain.validator;

import festival.domain.User;
import festival.domain.exceptions.ValidationException;


public class UserValidator implements Validator<User> {

    @Override
    public void validate(User user) throws ValidationException {

        String exp = "";

        String email = user.getEmail();
        if (email.equals("") || !email.contains("@") || email.indexOf("@") == email.length())
            exp += "Invalid email address!\n";

        if (user.getPassword().equals(""))
            exp += "Invalid password!\n";

        if (exp.length() > 0)
            throw new ValidationException(exp);

    }

}
