package festival.domain.observers;

public interface IObserver {
    void ticketSold(Long showId, Integer seatsCount);
}

