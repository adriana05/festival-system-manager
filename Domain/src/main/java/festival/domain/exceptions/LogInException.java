package festival.domain.exceptions;

public class LogInException extends Exception {
    public LogInException() {
        super("sign in error");
    }

    public LogInException(String message) {
        super(message);
    }
}
