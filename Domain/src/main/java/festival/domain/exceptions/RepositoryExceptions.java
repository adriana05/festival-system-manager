package festival.domain.exceptions;

public class RepositoryExceptions extends RuntimeException{
    public RepositoryExceptions() {
    }
    public RepositoryExceptions(String message) {
        super(message);
    }
}
