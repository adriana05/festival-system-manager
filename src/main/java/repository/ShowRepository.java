package repository;

import model.Show;

import java.time.LocalDateTime;

public interface ShowRepository extends Repository<Long, Show> {
    Iterable<Show> filterByDate(LocalDateTime dateTime);
}
