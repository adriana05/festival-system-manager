package repository;

import model.User;

public interface UserRepository extends Repository<Long, User> {

    User filterByEmailPassword(String email, String password);
}
