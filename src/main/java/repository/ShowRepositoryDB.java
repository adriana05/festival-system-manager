package repository;

import model.Show;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repository.exceptions.RepositoryExceptions;
import utils.JdbcUtils;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ShowRepositoryDB implements ShowRepository {

    private static final Logger logger = LogManager.getLogger();
    JdbcUtils jdbcUtils;

    public ShowRepositoryDB(JdbcUtils jdbcUtils) {
        logger.info("Initializing ShowRepositoryDB with properties: {} ", jdbcUtils.getProperties());
        this.jdbcUtils = jdbcUtils;
    }

    @Override
    public Iterable<Show> filterByDate(LocalDateTime dateTime) {
        logger.traceEntry();

        Connection connection = jdbcUtils.getConnection();
        List<Show> shows = new ArrayList<>();

        try(PreparedStatement statement = connection.prepareStatement("select  * from shows where datetime(DATE(date/1000,'unixepoch')) = ?")) {
            logger.traceEntry("The date is : {}", dateTime.toString()) ;
            statement.setString(1, dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                long id = result.getLong(1);
                String location = result.getString(2);
                int ticketsSold = result.getInt(3);
                int ticketsAvailable = result.getInt(4);
                LocalDateTime localDateTime = result.getTimestamp(5).toLocalDateTime();
                String artist = result.getString(6);

                Show show = new Show(localDateTime, location, ticketsSold, ticketsAvailable, artist);
                show.setID(id);

                shows.add(show);
            }
        } catch (SQLException ex) {
            logger.error(ex);
            throw new RepositoryExceptions(ex.getMessage());
        }

        logger.traceExit(shows);
        return shows;

    }

    @Override
    public Show findOne(Long aLong) {
        logger.traceEntry();

        if (aLong == null) {
            logger.error("FindOneFunction: Id cannot be null");
            throw new RepositoryExceptions("FindOneFunction: Id cannot be null");
        }
        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("select * from shows where id = ?")) {
            preparedStatement.setLong(1, aLong);
            try (ResultSet result = preparedStatement.executeQuery()) {
                if (result.next()) {
                    long id = result.getLong(1);
                    String location = result.getString(2);
                    int ticketsSold = result.getInt(3);
                    int ticketsAvailable = result.getInt(4);
                    LocalDateTime localDateTime = result.getTimestamp(5).toLocalDateTime();
                    String artist = result.getString(6);

                    Show show = new Show(localDateTime, location, ticketsSold, ticketsAvailable, artist);
                    show.setID(id);

                    logger.traceExit(show);
                    return show;
                }
            }
        } catch (SQLException exception) {
            throw new RepositoryExceptions(exception.getMessage());
        }

        return null;
    }

    @Override
    public Iterable<Show> findAll() {
        logger.traceEntry();

        List<Show> shows = new ArrayList<Show>();

        Connection conn = jdbcUtils.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement("select * from shows")) {
            try (ResultSet result = preparedStatement.executeQuery()) {
                while (result.next()) {
                    long id = result.getLong(1);
                    String location = result.getString(2);
                    int ticketsSold = result.getInt(3);
                    int ticketsAvailable = result.getInt(4);
                    LocalDateTime localDateTime = result.getTimestamp(5).toLocalDateTime();
                    String artist = result.getString(6);

                    Show show = new Show(localDateTime, location, ticketsSold, ticketsAvailable, artist);
                    show.setID(id);

                    shows.add(show);

                }

            }
        } catch (SQLException exception) {
            logger.error(exception);
            throw new RepositoryExceptions(exception.getMessage());
        }
        logger.traceExit();
        return shows;
    }

    @Override
    public Show save(Show entity) {
        logger.traceEntry();

        if (entity == null) {
            logger.error("SaveFunction: Entity cannot be null");
            throw new RepositoryExceptions("SaveFunction: Entity cannot be null!");
        }

        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("insert into shows(location, ticketsSold,ticketsAvailable, date, artist) values(?, ?, ?, ?, ?)")) {
            preparedStatement.setString(1, entity.getLocation());
            preparedStatement.setInt(2, entity.getNumberTicketsSold());
            preparedStatement.setInt(3, entity.getNumberTicketsAvailable());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(entity.getTime()));
            preparedStatement.setString(5, entity.getArtist());

            preparedStatement.execute();


        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }

        logger.traceExit();
        return entity;

    }

    @Override
    public Show delete(Long aLong) {
        logger.traceEntry();

        if (aLong == null) {
            logger.error("DeleteFunction: Id cannot be null");
            throw new RepositoryExceptions("DeleteFunction: Id cannot be null");
        }

        Show entity = findOne(aLong);
        if (entity == null) {
            logger.error("DeleteFunction: Entity not found");
            throw new RepositoryExceptions("DeleteFunction: Entity not found");
        }

        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("delete from shows where id =?")) {
            preparedStatement.setLong(1, aLong);

            preparedStatement.execute();

        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }
        return entity;
    }

    @Override
    public Show update(Show entity) {
        logger.traceEntry();

        if (entity == null) {
            logger.error("SaveFunction: Entity cannot be null");
            throw new RepositoryExceptions("SaveFunction: Entity cannot be null!");
        }

        Show show = findOne(entity.getID());
        if(show == null){
            logger.error("UpdateFunction: Entity not found");
            throw new RepositoryExceptions("UpdateFunction: Entity not found");
        }


        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("update shows set location = ?, ticketsSold = ?, ticketsAvailable = ?, date = ?, artist = ? where id = ?")) {
            preparedStatement.setLong(6, entity.getID());
            preparedStatement.setString(1, entity.getLocation());
            preparedStatement.setInt(2, entity.getNumberTicketsSold());
            preparedStatement.setInt(3, entity.getNumberTicketsAvailable());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(entity.getTime()));
            preparedStatement.setString(5, entity.getArtist());

            preparedStatement.executeUpdate();


        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }

        logger.traceExit(show);
        return show;
    }

    @Override
    public int size() {
        logger.traceEntry();

        Connection con = jdbcUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("select count(*) as [SIZE] from shows")) {
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    logger.traceExit(result.getInt("SIZE"));
                    return result.getInt("SIZE");
                }
            }
        } catch (SQLException ex) {
            logger.error(ex);
            throw new RepositoryExceptions(ex.getMessage());

        }
        return 0;
    }
}
