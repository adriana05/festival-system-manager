package repository.exceptions;

public class RepositoryExceptions extends RuntimeException{
    public RepositoryExceptions() {
    }
    public RepositoryExceptions(String message) {
        super(message);
    }
}
