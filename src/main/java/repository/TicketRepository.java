package repository;

import model.Ticket;

public interface TicketRepository extends Repository<Long, Ticket> {
}
