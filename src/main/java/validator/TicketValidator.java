package validator;

import model.Ticket;

public class TicketValidator implements Validator<Ticket> {

    @Override
    public void validate(Ticket ticket) throws ValidationException {

        String exp = "";

        if(ticket.getCustomerName().equals(""))
            exp += "Invalid customer's name!\n";

        if(ticket.getNumberTicketsBought() < 0)
            exp += "Invalid seats count!\n";

        if(ticket.getShow() == null)
            exp += "Invalid show!\n";

        if (exp.length() > 0)
            throw new ValidationException(exp);

    }

}
