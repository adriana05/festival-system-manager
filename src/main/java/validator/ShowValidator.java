package validator;

import model.Show;

import java.time.LocalDateTime;

public class ShowValidator implements Validator<Show>{

    @Override
    public void validate(Show show) throws ValidationException {

        String exp = "";

        if(show.getArtist().equals(""))
            exp += "Invalid Artist!\n";

        if(show.getTime().compareTo(LocalDateTime.now()) < 0)
            exp += "Invalid date!\n";

        if(show.getLocation().equals(""))
            exp += "Invalid place!\n";

        if(show.getNumberTicketsAvailable() < 0)
            exp += "Invalid available seats value!\n";

        if(show.getNumberTicketsSold() < 0)
            exp += "Invalid sold seats value!\n";

        if(exp.length() > 0)
            throw new ValidationException(exp);

    }

}
