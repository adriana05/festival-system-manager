package service;

import model.Show;
import model.Ticket;
import model.User;
import repository.ShowRepository;
import repository.TicketRepository;
import repository.UserRepository;
import validator.Validator;

import java.time.LocalDate;

public class Service {
    private ShowRepository showRepository;
    private Validator<Show> showValidator;
    private TicketRepository ticketRepository;
    private Validator<Ticket> ticketValidator;
    private UserRepository userRepository;
    private Validator<User> userValidator;

    public Service(ShowRepository showRepository, Validator<Show> showValidator, TicketRepository ticketRepository, Validator<Ticket> ticketValidator, UserRepository userRepository, Validator<User> userValidator) {
        this.showRepository = showRepository;
        this.showValidator = showValidator;
        this.ticketRepository = ticketRepository;
        this.ticketValidator = ticketValidator;
        this.userRepository = userRepository;
        this.userValidator = userValidator;
    }
    private void updateSeats(Show show, int dif) {

        show.setNumberTicketsSold(show.getNumberTicketsSold() + dif);
        show.setNumberTicketsAvailable(show.getNumberTicketsAvailable() - dif);

        showRepository.update(show);

    }

    public Ticket saveTicket(Ticket ticket) {
        ticketValidator.validate(ticket);
        Ticket savedTicket = ticketRepository.save(ticket);
        updateSeats(ticket.getShow(), ticket.getNumberTicketsBought());
        return savedTicket;

    }

    public User login(String email, String password) {

        return userRepository.filterByEmailPassword(email, password);

    }

    public Iterable<Show> findAllShows() {

        return showRepository.findAll();

    }

    public Iterable<Show> findShowsOnSpecificDate(LocalDate date) {

        return showRepository.filterByDate(date.atStartOfDay());
    }

}
