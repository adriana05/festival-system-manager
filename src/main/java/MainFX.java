import controller.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Show;
import model.Ticket;
import model.User;
import repository.*;
import service.Service;
import utils.JdbcUtils;
import validator.ShowValidator;
import validator.TicketValidator;
import validator.UserValidator;
import validator.Validator;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class MainFX extends Application {

    private UserRepository userRepository;
    private ShowRepository showRepository;
    private TicketRepository ticketRepository;

    private Service service;

    private Validator<User> userValidator;
    private Validator<Show> showValidator;
    private Validator<Ticket> ticketValidator;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Properties properties = new Properties();
        try {
            properties.load(new FileReader("bd.config"));
        } catch (IOException e) {
            System.out.println("Cannot find bd.config " + e);
        }
        JdbcUtils jdbcUtils = new JdbcUtils(properties);

        userRepository = new UserRepositoryDB(jdbcUtils);
        showRepository = new ShowRepositoryDB(jdbcUtils);
        ticketRepository = new TicketRepositoryDB(jdbcUtils);

        userValidator = new UserValidator();
        showValidator = new ShowValidator();
        ticketValidator = new TicketValidator();

        service = new Service(showRepository,showValidator,ticketRepository,ticketValidator,userRepository,userValidator);

        initStage();

    }

    void initStage() throws IOException {

        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/gui/login.fxml"));
        AnchorPane root=loader.load();

        LoginController loginController = loader.getController();
        loginController.setEnvironment(stage, service);

        stage.setScene(new Scene(root));

        stage.setTitle("Login");
        stage.show();

    }

}
