package model;

public class Entity<ID> {
    private ID id;

    public ID getID() {
        return id;
    }

    public void setID(ID Id) {
        this.id = Id;
    }
}
