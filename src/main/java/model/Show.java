package model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Show extends Entity<Long> {
    private LocalDateTime time;
    private String location;
    private int numberTicketsSold;
    private int numberTicketsAvailable;
    private String artist;


    public Show(LocalDateTime time, String location, int numberTicketsSold, int numberTicketsAvailable, String artist) {
        this.time = time;
        this.location = location;
        this.numberTicketsSold = numberTicketsSold;
        this.numberTicketsAvailable = numberTicketsAvailable;
        this.artist = artist;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNumberTicketsSold() {
        return numberTicketsSold;
    }

    public void setNumberTicketsSold(int numberTicketsSold) {
        this.numberTicketsSold = numberTicketsSold;
    }

    public int getNumberTicketsAvailable() {
        return numberTicketsAvailable;
    }

    public void setNumberTicketsAvailable(int numberTicketsAvailable) {
        this.numberTicketsAvailable = numberTicketsAvailable;
    }

    @Override
    public String toString() {
        return "Show{" +
                "time=" + time +
                ", location='" + location + '\'' +
                ", numberTicketsSold=" + numberTicketsSold +
                ", numberTicketsAvailable=" + numberTicketsAvailable +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Show)) return false;
        Show show = (Show) o;
        return getNumberTicketsSold() == show.getNumberTicketsSold() &&
                getNumberTicketsAvailable() == show.getNumberTicketsAvailable() &&
                getTime().equals(show.getTime()) &&
                getLocation().equals(show.getLocation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTime(), getLocation(), getNumberTicketsSold(), getNumberTicketsAvailable());
    }

    public String getTimeToString() {
        return time.format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    public String getDateToString(){
        return time.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
    }
}
