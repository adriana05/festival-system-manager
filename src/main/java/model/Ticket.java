package model;

import java.util.Objects;

public class Ticket extends Entity<Long> {
    private String customerName;
    private int numberTicketsBought;
    private Show show;

    public Ticket(String customerName, int numberTicketsBought, Show show) {
        this.customerName = customerName;
        this.numberTicketsBought = numberTicketsBought;
        this.show = show;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getNumberTicketsBought() {
        return numberTicketsBought;
    }

    public void setNumberTicketsBought(int numberTicketsBought) {
        this.numberTicketsBought = numberTicketsBought;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;
        Ticket ticket = (Ticket) o;
        return getNumberTicketsBought() == ticket.getNumberTicketsBought() &&
                Objects.equals(getCustomerName(), ticket.getCustomerName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCustomerName(), getNumberTicketsBought());
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "customerName='" + customerName + '\'' +
                ", numberTicketsBought=" + numberTicketsBought +
                '}';
    }
}
