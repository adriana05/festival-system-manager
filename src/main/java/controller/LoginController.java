package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.User;
import repository.exceptions.RepositoryExceptions;
import service.Service;

import java.io.IOException;

public class LoginController {

    public PasswordField textFieldPassword;
    public TextField textFieldEmail;
    public Label labelLoginError;

    private Service service;

    private Stage stage;

    public void setEnvironment(Stage stage, Service service) {

        this.stage = stage;
        this.service = service;

    }


    public void handleLogin(ActionEvent actionEvent) throws IOException {

        String email = textFieldEmail.getText();
        String password = textFieldPassword.getText();
        try {
            User loggedInUser = service.login(email, password);
        } catch (RepositoryExceptions ex) {
            labelLoginError.setText(ex.getMessage());
            return;
        }

        initMainWindow();

    }

    void initMainWindow() throws IOException {

        Stage mainStage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/gui/application.fxml"));
        AnchorPane root = loader.load();

        MainController mainController = loader.getController();
        mainController.setEnvironment(mainStage, service);

        mainStage.setScene(new Scene(root));
        mainStage.setTitle("View");

        stage.close();

        mainStage.show();

    }

}
