package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Show;
import model.Ticket;
import service.Service;
import validator.ValidationException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MainController {


    public TableView<Show> tableShows;
    public TableColumn<Show, String> showTableArtistColumn;
    public TableColumn<Show, String> showTableLocationColumn;
    public TableColumn<Show, String> showTableDateTimeColumn;
    public TableColumn<Show, Integer> showTableAvailableSeatsColumn;
    public TableColumn<Show, Integer> showTableSoldSeatsColumn;

    public TableView<Show> tableFilteredShows;
    public TableColumn<Show, String> filteredShowTableArtistColumn;
    public TableColumn<Show, String> filteredShowTableLocationColumn;
    public TableColumn<Show, String> filteredShowTableTimeColumn;
    public TableColumn<Show, Integer> filteredShowTableAvailableSeatsColumn;

    public Label labelFilterShowsError;
    public Label labelSellTicketError;

    public DatePicker datePickerForShows;
    public TextField textFieldSeats;
    public TextField textFieldArtist;
    public TextField textFieldDateTime;
    public TextField textFieldBuyerName;
    public TextField textFieldLocation;
    ObservableList<Show> modelShows = FXCollections.observableArrayList();
    ObservableList<Show> modelShowsByDate = FXCollections.observableArrayList();

    private Service service;
    private Stage stage;

    private Show clickedShow = null;
    private Long clickedShowId = (long) 0;

    public void setEnvironment(Stage stage, Service service){

        this.stage = stage;
        this.service = service;

        setTableShows();

    }

    private void setTableShowsRowColor(TableView<Show> table) {

        table.setRowFactory(tv -> new TableRow<Show>() {
            @Override
            protected void updateItem(Show item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setStyle("");
                } else if (item.getNumberTicketsAvailable() <= 0) {
                    setStyle("-fx-background-color: #ff0000");
                } else {
                    setStyle("");
                }
            }
        });

    }

    @FXML
    public void initialize() {

        showTableArtistColumn.setCellValueFactory(new PropertyValueFactory<>("artist"));
        showTableLocationColumn.setCellValueFactory(new PropertyValueFactory<>("location"));
        showTableDateTimeColumn.setCellValueFactory(new PropertyValueFactory<>("dateToString"));
        showTableAvailableSeatsColumn.setCellValueFactory(new PropertyValueFactory<>("numberTicketsAvailable"));
        showTableSoldSeatsColumn.setCellValueFactory(new PropertyValueFactory<>("numberTicketsSold"));
        tableShows.setItems(modelShows);

        setTableShowsRowColor(tableShows);

        filteredShowTableArtistColumn.setCellValueFactory((new PropertyValueFactory<>("artist")));
        filteredShowTableLocationColumn.setCellValueFactory((new PropertyValueFactory<>("location")));
        filteredShowTableTimeColumn.setCellValueFactory((new PropertyValueFactory<>("timeToString")));
        filteredShowTableAvailableSeatsColumn.setCellValueFactory((new PropertyValueFactory<>("numberTicketsAvailable")));

        setTableShowsRowColor(tableFilteredShows);

    }

    private void setTableShows() {

        this.modelShows.setAll(StreamSupport.stream(service.findAllShows().spliterator(), false).collect(Collectors.toList()));

    }

    private void setFilteredTableShows() {

        Show show = tableFilteredShows.getSelectionModel().getSelectedItem();
        if (show == null)
            return;

        LocalDate date = show.getTime().toLocalDate();
        if (date == null)
            return;

        this.modelShowsByDate.setAll(StreamSupport.stream(service.findShowsOnSpecificDate(date).spliterator(), false).collect(Collectors.toList()));

    }

    public void handleFilterByDate(ActionEvent actionEvent) {

        LocalDate date = datePickerForShows.getValue();

        if (date == null) {

            labelFilterShowsError.setText("You need to pick a date!");
            return;

        }

        labelFilterShowsError.setText("");
        tableShows.setVisible(false);
        tableFilteredShows.setVisible(true);

        modelShowsByDate.setAll(StreamSupport.stream(service.findShowsOnSpecificDate(date).spliterator(), false).collect(Collectors.toList()));
        tableFilteredShows.setItems(modelShowsByDate);

    }

    public void handleShowAll(ActionEvent actionEvent) {

        tableFilteredShows.setVisible(false);
        tableShows.setVisible(true);

    }

    public void handleLogout(ActionEvent actionEvent) throws IOException {

        Stage loginStage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/gui/login.fxml"));
        AnchorPane root = loader.load();

        LoginController loginController = loader.getController();
        loginController.setEnvironment(loginStage, service);

        loginStage.setScene(new Scene(root));
        //stage.initStyle(StageStyle.UNDECORATED);
        loginStage.setTitle("Login");

        stage.close();

        loginStage.show();

    }

    public void handleSelectionChangedShowsTable(MouseEvent mouseEvent) {

        Show show = tableShows.getSelectionModel().getSelectedItem();
        if (show == null)
            return;

        textFieldArtist.setText(show.getArtist());
        textFieldDateTime.setText(show.getDateToString());
        textFieldLocation.setText(show.getLocation());
        clickedShow = show;

    }

    public void handleSelectionChangedFilteredShowsTable(MouseEvent mouseEvent) {

        Show show = tableFilteredShows.getSelectionModel().getSelectedItem();
        if (show == null)
            return;

        textFieldArtist.setText(show.getArtist());
        textFieldDateTime.setText(show.getDateToString());
        textFieldLocation.setText(show.getLocation());
        clickedShow = show;

    }

    private Ticket verifySellTicket() {

        if (clickedShow == null) {

            labelSellTicketError.setText("Pick a show from the list!");
            return null;

        }

        String buyer = textFieldBuyerName.getText();
        String seatsCountString = textFieldSeats.getText();

        if (buyer.equals("") || seatsCountString.equals("")) {

            labelSellTicketError.setText("Fill all ticket info!");
            return null;

        }

        int seatsCount = Integer.parseInt(seatsCountString);

        if (seatsCount > clickedShow.getNumberTicketsAvailable()) {

            labelSellTicketError.setText("Not enough available seats!");
            return null;

        }

        return new Ticket(buyer, seatsCount, clickedShow);

    }

    public void handleSellTicket(ActionEvent actionEvent) {

        Ticket ticket = verifySellTicket();
        if (ticket == null)
            return;

        try {
            service.saveTicket(ticket);
        } catch (ValidationException ex) {
            labelSellTicketError.setText(ex.getMessage());
            return;
        }

        setTableShows();
        setFilteredTableShows();

        labelSellTicketError.setText("");

    }

}
