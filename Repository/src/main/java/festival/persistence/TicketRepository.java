package festival.persistence;


import festival.domain.Ticket;

public interface TicketRepository extends Repository<Long, Ticket> {
}
