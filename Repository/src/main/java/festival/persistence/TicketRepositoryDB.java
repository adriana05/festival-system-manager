package festival.persistence;

import festival.domain.Show;
import festival.domain.Ticket;
import festival.domain.exceptions.RepositoryExceptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TicketRepositoryDB implements TicketRepository {

    private static final Logger logger = LogManager.getLogger();
    JdbcUtils jdbcUtils;

    public TicketRepositoryDB() {
        Configuration.loadProperties(".\\Repository\\persistence.config");
        Configuration.logger.info("Initializing UserRepository with {} ", Configuration.properties);
        this.jdbcUtils=new JdbcUtils(Configuration.properties);
    }

    private Ticket getTicket(ResultSet result, Long aLong, Connection conn) throws SQLException {

        String customerName = result.getString(2);
        int seatsBought = result.getInt(3);
        long idShow = result.getLong(4);
        Show show;

        try (PreparedStatement preparedStatementShow = conn.prepareStatement("select * from shows where id = ?")) {
            preparedStatementShow.setLong(1, idShow);

            try (ResultSet rs = preparedStatementShow.executeQuery()) {
                String location = rs.getString(2);
                int ticketsSold = rs.getInt(3);
                int ticketsAvailable = rs.getInt(4);
                LocalDateTime localDateTime = rs.getTimestamp(5).toLocalDateTime();
                String artist = rs.getString(6);

                show = new Show(localDateTime, location, ticketsSold, ticketsAvailable, artist);
                show.setID(idShow);
            }
        }

        Ticket ticket = new Ticket(customerName, seatsBought, show);
        ticket.setID(aLong);

        return ticket;


    }

    @Override
    public Ticket findOne(Long aLong) {
        logger.traceEntry();

        if (aLong == null) {
            logger.error("FindOneFunction: Id cannot be null");
            throw new RepositoryExceptions("FindOneFunction: Id cannot be null");
        }
        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("select * from tickets where id = ?")) {
            preparedStatement.setLong(1, aLong);

            try (ResultSet result = preparedStatement.executeQuery()) {
                if (result.next()) {
                    Ticket ticket = getTicket(result, aLong, conn);

                    logger.traceExit(ticket);
                    return ticket;
                }
            }
        } catch (SQLException exception) {
            logger.error(exception);
            throw new RepositoryExceptions(exception.getMessage());
        }

        logger.traceExit();
        return null;
    }

    @Override
    public Iterable<Ticket> findAll() {
        logger.traceEntry();

        List<Ticket> tickets = new ArrayList<>();

        Connection conn = jdbcUtils.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement("select * from tickets")) {
            try (ResultSet result = preparedStatement.executeQuery()) {
                while (result.next()) {
                    long id = result.getLong(1);

                    Ticket ticket = getTicket(result, id, conn);

                    tickets.add(ticket);
                }
            }
        } catch (SQLException exception) {
            logger.error(exception);
            throw new RepositoryExceptions(exception.getMessage());
        }
        logger.traceExit(tickets);
        return tickets;
    }

    @Override
    public Ticket save(Ticket entity) {
        logger.traceEntry();

        if (entity == null) {
            logger.error("SaveFunction: Entity cannot be null");
            throw new RepositoryExceptions("SaveFunction: Entity cannot be null!");
        }

        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("insert into tickets(customerName, seatsBought, idShow) values(?, ?, ?)")) {
            preparedStatement.setString(1, entity.getCustomerName());
            preparedStatement.setInt(2, entity.getNumberTicketsBought());
            preparedStatement.setLong(3, entity.getShow().getID());

            preparedStatement.execute();


        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }

        logger.traceExit(entity);
        return entity;

    }

    @Override
    public Ticket delete(Long aLong) {
        logger.traceEntry();

        if (aLong == null) {
            logger.error("DeleteFunction: Id cannot be null");
            throw new RepositoryExceptions("DeleteFunction: Id cannot be null");
        }

        Ticket entity = findOne(aLong);
        if (entity == null) {
            logger.error("DeleteFunction: Entity not found");
            throw new RepositoryExceptions("DeleteFunction: Entity not found");
        }

        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("delete from tickets where id =?")) {
            preparedStatement.setLong(1, aLong);

            preparedStatement.execute();

        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }
        logger.traceExit(entity);
        return entity;
    }

    @Override
    public Ticket update(Ticket entity) {
        logger.traceEntry();

        if (entity == null) {
            logger.error("SaveFunction: Entity cannot be null");
            throw new RepositoryExceptions("SaveFunction: Entity cannot be null!");
        }

        Ticket ticket = findOne(entity.getID());
        if (ticket == null) {
            logger.error("UpdateFunction: Entity not found");
            throw new RepositoryExceptions("UpdateFunction: Entity not found");
        }

        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("update tickets set customerName = ?, seatsBought = ?, idShow = ? where id = ?")) {
            preparedStatement.setLong(4, entity.getID());
            preparedStatement.setString(1, entity.getCustomerName());
            preparedStatement.setInt(2, entity.getNumberTicketsBought());
            preparedStatement.setLong(3, entity.getShow().getID());

            preparedStatement.executeUpdate();


        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }

        logger.traceExit(entity);
        return ticket;
    }

    @Override
    public int size() {
        logger.traceEntry();

        Connection con = jdbcUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("select count(*) as [SIZE] from tickets")) {
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    logger.traceExit(result.getInt("SIZE"));
                    return result.getInt("SIZE");
                }
            }
        } catch (SQLException ex) {
            logger.error(ex);
            throw new RepositoryExceptions(ex.getMessage());

        }
        return 0;
    }
}
