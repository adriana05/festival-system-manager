package festival.persistence;

import festival.domain.User;
import festival.domain.exceptions.RepositoryExceptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryDB implements UserRepository {

    private static final Logger logger = LogManager.getLogger();
    JdbcUtils jdbcUtils;

    public UserRepositoryDB() {
        Configuration.loadProperties(".\\Repository\\persistence.config");
        Configuration.logger.info("Initializing UserRepository with {} ", Configuration.properties);
        this.jdbcUtils=new JdbcUtils(Configuration.properties);
    }


    @Override
    public User findOne(Long aLong) {
        logger.traceEntry();

        if (aLong == null) {
            logger.error("FindOneFunction: Id cannot be null");
            throw new RepositoryExceptions("FindOneFunction: Id cannot be null");
        }
        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("select * from users where id = ?")) {
            preparedStatement.setLong(1, aLong);
            try (ResultSet result = preparedStatement.executeQuery()) {
                if (result.next()) {
                    String email = result.getString(1);
                    String password = result.getString(2);

                    User user = new User(email, password);
                    user.setID(aLong);

                    logger.traceExit(user);
                    return user;
                }
            }
        } catch (SQLException exception) {
            logger.error(exception);
            throw new RepositoryExceptions(exception.getMessage());
        }
        logger.traceExit();
        return null;
    }

    @Override
    public Iterable<User> findAll() {
        logger.traceEntry();

        List<User> users = new ArrayList<>();

        Connection conn = jdbcUtils.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement("select * from users")) {
            try (ResultSet result = preparedStatement.executeQuery()) {
                while (result.next()) {
                    long id = result.getLong(1);
                    String email = result.getString(2);
                    String password = result.getString(3);

                    User user = new User(email, password);
                    user.setID(id);

                    users.add(user);
                }
            }
        } catch (SQLException exception) {
            logger.error(exception);
            throw new RepositoryExceptions(exception.getMessage());
        }
        logger.traceExit(users);
        return users;
    }

    @Override
    public User save(User entity) {
        logger.traceEntry();

        if (entity == null) {
            logger.error("SaveFunction: Entity cannot be null");
            throw new RepositoryExceptions("SaveFunction: Entity cannot be null!");
        }

        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("insert into users(email, password) values(?, ?)")) {
            preparedStatement.setString(1, entity.getEmail());
            preparedStatement.setString(2, entity.getPassword());

            preparedStatement.execute();


        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }

        logger.traceExit();
        return entity;

    }

    @Override
    public User delete(Long aLong) {
        logger.traceEntry();

        if (aLong == null) {
            logger.error("DeleteFunction: Id cannot be null");
            throw new RepositoryExceptions("DeleteFunction: Id cannot be null");
        }

        User entity = findOne(aLong);
        if (entity == null) {
            logger.error("DeleteFunction: Entity not found");
            throw new RepositoryExceptions("DeleteFunction: Entity not found");
        }

        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("delete from users where id =?")) {
            preparedStatement.setLong(1, aLong);

            preparedStatement.execute();

        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }
        logger.traceExit(entity);
        return entity;
    }

    @Override
    public User update(User entity) {
        logger.traceEntry();

        if (entity == null) {
            logger.error("SaveFunction: Entity cannot be null");
            throw new RepositoryExceptions("SaveFunction: Entity cannot be null!");
        }

        User user = findOne(entity.getID());
        if (user == null) {
            logger.error("UpdateFunction: Entity not found");
            throw new RepositoryExceptions("UpdateFunction: Entity not found");
        }


        Connection conn = jdbcUtils.getConnection();

        try (PreparedStatement preparedStatement = conn.prepareStatement("update users set email = ?, password = ? where id = ?")) {
            preparedStatement.setLong(3, entity.getID());
            preparedStatement.setString(1, entity.getEmail());
            preparedStatement.setString(2, entity.getPassword());

            preparedStatement.executeUpdate();


        } catch (SQLException sqlEx) {
            logger.error(sqlEx);
            throw new RepositoryExceptions(sqlEx.getMessage());
        }

        logger.traceExit(user);
        return user;
    }

    @Override
    public int size() {
        logger.traceEntry();

        Connection con = jdbcUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("select count(*) as [SIZE] from users")) {
            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    logger.traceExit(result.getInt("SIZE"));
                    return result.getInt("SIZE");
                }
            }
        } catch (SQLException ex) {
            logger.error(ex);
            throw new RepositoryExceptions(ex.getMessage());

        }
        return 0;
    }

    @Override
    public User filterByEmailPassword(String email, String password) {
        logger.traceEntry();

        Connection con = jdbcUtils.getConnection();

        try (PreparedStatement preStmt = con.prepareStatement("select * from users where email = ? and password = ?")) {
            preStmt.setString(1,email);
            preStmt.setString(2,password);

            try (ResultSet result = preStmt.executeQuery()) {
                if (result.next()) {
                    long id  = result.getLong(1);
                    User user = new User(email,password);
                    user.setID(id);

                    logger.traceExit(user);
                    return user;

                }
                else{
                    logger.error("User invalid");
                    throw new RepositoryExceptions("User invalid");
                }
            }
        } catch (SQLException ex) {
            logger.error(ex);
            throw new RepositoryExceptions(ex.getMessage());

        }
    }
}
