package festival.persistence;



import festival.domain.Show;

import java.time.LocalDateTime;

public interface ShowRepository extends Repository<Long, Show> {
    Iterable<Show> filterByDate(LocalDateTime dateTime);
}
