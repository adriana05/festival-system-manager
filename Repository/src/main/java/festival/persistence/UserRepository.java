package festival.persistence;


import festival.domain.User;

public interface UserRepository extends Repository<Long, User> {

    User filterByEmailPassword(String email, String password);
}
