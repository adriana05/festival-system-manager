package festival.networking.dataTransferObjects;


import festival.domain.Show;
import festival.domain.Ticket;
import festival.domain.User;
import festival.networking.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DtoUtils {

    public static User toUser(UserDto userDto) {
        Configuration.logger.traceEntry("entering with {}", userDto);

        String username = userDto.getUsername();
        String password = userDto.getPassword();
        User user = new User(username, password);

        Configuration.logger.traceExit(user);
        return user;
    }

    public static UserDto toDto(User user) {
        Configuration.logger.traceEntry("entering with {}", user);

        Long id = user.getID();
        String email = user.getEmail();
        String password = user.getPassword();
        UserDto userDto = new UserDto(id, email, password);

        Configuration.logger.traceExit(userDto);
        return userDto;
    }

    public static String toEmail(EmailDto usernameDto) {
        Configuration.logger.traceEntry("entering with {}", usernameDto);

        String username = usernameDto.getValue();

        Configuration.logger.traceExit(username);
        return username;
    }

    public static DateDto toDto(LocalDate date) {
        Configuration.logger.traceEntry("entering with {}", date);

        DateDto dateDto = new DateDto(date);

        Configuration.logger.traceExit(dateDto);
        return dateDto;
    }

    public static EmailDto toDto(String email) {
        Configuration.logger.traceEntry("entering with {}", email);

        EmailDto emailDto = new EmailDto(email);

        Configuration.logger.traceExit(emailDto);
        return emailDto;
    }

    public static Show toShow(ShowDto showDto) {
        Configuration.logger.traceEntry("entering with {}", showDto);

        Long id = showDto.getId();
        String artist = showDto.getArtist();
        LocalDateTime dateTime = showDto.getDateTime();
        String place = showDto.getPlace();
        Integer availableSeats = showDto.getAvailableSeats();
        Integer soldSeats = showDto.getSoldSeats();
        Show show = new Show(dateTime, place, soldSeats, availableSeats, artist);
        show.setID(id);

        Configuration.logger.traceExit(show);
        return show;
    }

    public static ShowDto toDto(Show show) {
        Configuration.logger.traceEntry("entering with {}", show);

        Long id = show.getID();
        String artist = show.getArtist();
        LocalDateTime dateTime = show.getTime();
        String place = show.getLocation();
        Integer availableSeats = show.getNumberTicketsAvailable();
        Integer soldSeats = show.getNumberTicketsSold();
        ShowDto showDto = new ShowDto(id, artist, dateTime, place, availableSeats, soldSeats);

        Configuration.logger.traceExit(showDto);
        return showDto;
    }

    public static Collection<Show> toShowCollection(ShowCollectionDto showCollectionDto) {
        Configuration.logger.traceEntry("entering with {}", showCollectionDto);

        Collection<Show> shows = new ArrayList<>();
        for (ShowDto showDto : showCollectionDto.getShows()) {
            shows.add(toShow(showDto));
        }

        Configuration.logger.traceExit(shows);
        return shows;
    }

    public static ShowCollectionDto toDto(Collection<Show> showsCollection) {
        Configuration.logger.traceEntry("entering with {}", showsCollection);

        List<Show> showList = new ArrayList<>(showsCollection);
        ShowDto[] showDtos = new ShowDto[showList.size()];
        for (int index = 0; index < showList.size(); index++) {
            showDtos[index] = toDto(showList.get(index));
        }
        ShowCollectionDto gameCollectionDto = new ShowCollectionDto(showDtos);

        Configuration.logger.traceExit(gameCollectionDto);
        return gameCollectionDto;
    }

    public static TicketSellingDto toDto(Ticket ticket) {
        Configuration.logger.traceEntry("entering with {} {}", ticket);

        ShowDto showDto = toDto(ticket.getShow());
        TicketSellingDto ticketSellingDto = new TicketSellingDto(showDto, ticket.getCustomerName(), ticket.getNumberTicketsBought());

        Configuration.logger.traceExit(ticketSellingDto);
        return ticketSellingDto;
    }

    public static TicketSoldDto toDto(Long showId, Integer seatsCount) {
        Configuration.logger.traceEntry("entering with {} {}", showId, seatsCount);

        TicketSoldDto seatsSoldDto = new TicketSoldDto(showId, seatsCount);

        Configuration.logger.traceExit(seatsSoldDto);
        return seatsSoldDto;
    }

}
