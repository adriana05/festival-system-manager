package festival.networking.dataTransferObjects;

import java.io.Serializable;

public class UserDto implements Serializable {
    private final Long id;
    private final String username;
    private final String password;

    public UserDto(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "UserDto[" + getUsername() + " " + getPassword() + "]";
    }
}
