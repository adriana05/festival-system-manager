package festival.networking.dataTransferObjects;

import java.io.Serializable;

public class EmailDto implements Serializable {
    private final String value;

    public EmailDto(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "EmailDto[" + getValue() + "]";
    }
}
