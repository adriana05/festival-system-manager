package festival.networking.dataTransferObjects;

import java.io.Serializable;
import java.time.LocalDate;

public class DateDto implements Serializable {
    private final LocalDate value;

    public DateDto(LocalDate value) {
        this.value = value;
    }

    public LocalDate getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "DateDto['" + value + ']';
    }
}
