package festival.networking.rpc;

import festival.domain.Show;
import festival.domain.Ticket;
import festival.domain.User;
import festival.domain.exceptions.NetworkingException;
import festival.domain.observers.IObserver;
import festival.networking.Configuration;
import festival.networking.dataTransferObjects.*;
import festival.service.IService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Worker implements Runnable, IObserver {

    private final IService services;
    private final Socket connection;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private volatile boolean connected;

    public Worker(IService services, Socket connection) {
        this.services = services;
        this.connection = connection;
    }

    private void ensureConnection() throws NetworkingException {
        if (input != null && output != null)
            return;
        if (connection == null)
            throw new NetworkingException("lost connection to client");

        try {
            output = new ObjectOutputStream(connection.getOutputStream());
            output.flush();
            input = new ObjectInputStream(connection.getInputStream());
            connected = true;
            Configuration.logger.trace("ensured connection {} {} {}", connection, input, output);
        } catch (IOException exception) {
            throw new NetworkingException("could not ensure connection");
        }
    }

    private void closeConnection() {
        try {
            input.close();
            output.close();
            connection.close();
            Configuration.logger.trace("closed connection");
        } catch (IOException exception) {
            Configuration.logger.error("could not close connection");
        }
    }

    @Override
    public void run() {
        Configuration.logger.traceEntry();

        try {
            ensureConnection();
            while (connected) {
                try {
                    ensureConnection();
                    Configuration.logger.trace("waiting request");
                    Request request = (Request) input.readObject();
                    Configuration.logger.trace("request received {}", request);
                    Response response = handleRequest(request);
                    if (response != null)
                        sendResponse(response);
                    Configuration.logger.trace("request handled {} and response sent {}", request, response);
//                Thread.sleep(1000);
                } catch (Exception exception) {
                    Configuration.logger.error(exception);
                }
            }
            closeConnection();
        } catch (Exception exception) {
            Configuration.logger.error(exception);
        }

        Configuration.logger.traceExit();
    }

    private Response handleRequest(Request request) {
        Configuration.logger.traceEntry("entering with {}", request);

        String handlerName = "handle" + request.getType();
        try {
            Method method = this.getClass().getDeclaredMethod(handlerName, Request.class);

            Configuration.logger.traceExit();
            return (Response) method.invoke(this, request);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {

            Configuration.logger.traceExit();
            return new Response.Builder().setType(ResponseType.ERROR).setData("unknown request").build();
        }
    }

    private Response handleLOGIN(Request request) {
        Configuration.logger.traceEntry("entering with {}", request);

        Response response;
        try {
            User user = DtoUtils.toUser((UserDto) request.getData());
            services.login(user.getEmail(), user.getPassword(), this);
            response = new Response.Builder().setType(ResponseType.OK).setData(request.getData()).build();
        } catch (Exception exception) {
            response = new Response.Builder().setType(ResponseType.ERROR).setData(exception.getMessage()).build();
        }

        Configuration.logger.traceExit(response);
        return response;
    }

    private Response handleLOGOUT(Request request) {
        Configuration.logger.traceEntry("entering with {}", request);

        Response response;
        try {
            services.logout(DtoUtils.toEmail((EmailDto) request.getData()), this);
            connected = false;
            response = new Response.Builder().setType(ResponseType.OK).build();
        } catch (Exception exception) {
            response = new Response.Builder().setType(ResponseType.ERROR).setData(exception.getMessage()).build();
        }

        Configuration.logger.traceExit(response);
        return response;
    }

    private Response handleGET_ALL_SHOWS(Request request) {
        Configuration.logger.traceEntry("entering with {}", request);

        Response response;
        try {
            List<Show> showsCollection = StreamSupport.stream(services.findAllShows().spliterator(), false).collect(Collectors.toList());
            ShowCollectionDto showCollectionDto = DtoUtils.toDto(showsCollection);
            response = new Response.Builder().setType(ResponseType.OK).setData(showCollectionDto).build();
        } catch (Exception exception) {
            response = new Response.Builder().setType(ResponseType.ERROR).setData(exception.getMessage()).build();
        }

        Configuration.logger.traceExit(response);
        return response;
    }

    private Response handleSELL_TICKET(Request request) {
        Configuration.logger.traceEntry("entering with {}", request);

        Response response;
        try {
            TicketSellingDto ticketSellingDto = (TicketSellingDto) request.getData();
            Show show = DtoUtils.toShow(ticketSellingDto.getShowDto());
            services.saveTicket(new Ticket(ticketSellingDto.getClientName(), ticketSellingDto.getSeatsCount(), show));
            response = new Response.Builder().setType(ResponseType.OK).build();
        } catch (Exception exception) {
            response = new Response.Builder().setType(ResponseType.ERROR).setData(exception.getMessage()).build();
        }

        Configuration.logger.traceExit(response);
        return response;
    }

    private Response handleGET_SHOWS_ON_DATE(Request request) {
        Configuration.logger.traceEntry("entering with {}", request);

        Response response;
        try {
            DateDto dateDto = (DateDto) request.getData();
            LocalDate date = dateDto.getValue();

            List<Show> showsCollection = StreamSupport.stream(services.findShowsOnSpecificDate(date).spliterator(), false).collect(Collectors.toList());
            ShowCollectionDto showCollectionDto = DtoUtils.toDto(showsCollection);
            response = new Response.Builder().setType(ResponseType.OK).setData(showCollectionDto).build();
        } catch (Exception exception) {
            response = new Response.Builder().setType(ResponseType.ERROR).setData(exception.getMessage()).build();
        }

        Configuration.logger.traceExit(response);
        return response;
    }

    private void sendResponse(Response response) throws NetworkingException {
        Configuration.logger.traceEntry("entering with {}", response);

        ensureConnection();
        try {
            output.writeObject(response);
            output.flush();
            Configuration.logger.trace("sent response {}", response);
        } catch (IOException exception) {
            throw new NetworkingException("error sending response " + exception);
        }

        Configuration.logger.traceExit();
    }

    @Override
    public void ticketSold(Long showId, Integer seatsCount) {
        Configuration.logger.traceEntry("entering with {} {}", showId, seatsCount);

        try {
            TicketSoldDto seatsSoldDto = DtoUtils.toDto(showId, seatsCount);
            Response response = new Response.Builder().setType(ResponseType.SEATS_SOLD).setData(seatsSoldDto).build();
            sendResponse(response);
        } catch (Exception exception) {
            Configuration.logger.error(exception);
        }

        Configuration.logger.traceExit();
    }
}
