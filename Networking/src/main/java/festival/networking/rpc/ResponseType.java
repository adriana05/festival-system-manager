package festival.networking.rpc;

public enum ResponseType {
    OK,
    ERROR,
    SEATS_SOLD;
}
