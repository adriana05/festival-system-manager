package festival.networking.rpc;

public enum RequestType {
    LOGIN, LOGOUT, GET_ALL_SHOWS, SELL_TICKET, GET_SHOWS_ON_DATE;
}
