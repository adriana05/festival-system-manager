package festival.server;
import festival.domain.Show;
import festival.domain.Ticket;
import festival.domain.User;
import festival.domain.exceptions.ServerException;
import festival.domain.validator.ShowValidator;
import festival.domain.validator.TicketValidator;
import festival.domain.validator.UserValidator;
import festival.domain.validator.Validator;
import festival.persistence.*;
import festival.server.servers.RpcConcurrentServer;
import festival.server.servers.Server;
import festival.service.IService;
import festival.service.Service;


public class RpcServerStarter {

    private static final int defaultPort = 55555;

    private static int configureServer(){
        Configuration.logger.traceEntry();

        Configuration.loadProperties(".\\Server\\server.config");

        int serverPort = defaultPort;
        try {
            serverPort = Integer.parseInt(Configuration.properties.getProperty("server.port"));
        } catch (NumberFormatException exception) {
            Configuration.logger.error("wrong port number {}", exception.getMessage());
            Configuration.logger.warn("using default port {}", defaultPort);
        }
        Configuration.logger.info("starting server on port {}", serverPort);

        return serverPort;
    }

    public static void main(String[] args) {

        int serverPort = configureServer();

        UserRepository userRepository = new UserRepositoryDB();
        ShowRepository showRepository = new ShowRepositoryDB();
        TicketRepository ticketRepository = new TicketRepositoryDB();

        Validator<User> userValidator = new UserValidator();
        Validator<Show> showValidator = new ShowValidator();
        Validator<Ticket> ticketValidator = new TicketValidator();

        IService services = new Service(showRepository, showValidator, ticketRepository, ticketValidator, userRepository,userValidator);
        Configuration.logger.trace("created service {} instance", services);

        Server server = new RpcConcurrentServer(serverPort, services);
        Configuration.logger.trace("created server {} instance", server);
        try {
            server.start();
        } catch (ServerException exception) {
            Configuration.logger.error("could not start the server", exception);
        } finally {
            try {
                server.stop();
            } catch (ServerException exception) {
                Configuration.logger.error("could not stop the server", exception);
            }
        }

        Configuration.logger.traceExit();
    }
}
