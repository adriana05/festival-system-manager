package festival.server.servers;

import festival.networking.rpc.Worker;
import festival.server.Configuration;
import festival.service.IService;

import java.net.Socket;

public class RpcConcurrentServer extends ConcurrentServer {

    private final IService services;

    public RpcConcurrentServer(Integer port, IService services) {
        super(port);
        this.services = services;
    }

    @Override
    protected Thread createClientProxyThread(Socket connection) {
        Configuration.logger.traceEntry("entering with", connection);

        Worker clientProxy = new Worker(services, connection);
        Thread clientProxyThread = new Thread(clientProxy);

        Configuration.logger.traceExit(clientProxyThread);
        return clientProxyThread;
    }
}
