package festival.server.servers;

import festival.domain.exceptions.ServerException;
import festival.server.Configuration;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class Server {

    private final Integer port;
    private ServerSocket serverSocket = null;

    public Server(Integer port) {
        this.port = port;
    }

    public void start() throws ServerException {
        Configuration.logger.traceEntry();

        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                Configuration.logger.trace("Waiting for clients on {}", serverSocket);
                Socket clientSocket = serverSocket.accept();
                Configuration.logger.trace("Client connected {}", clientSocket);
                beginConversation(clientSocket);
            }
        } catch (IOException exception) {
            throw new ServerException("Error trying to connect to client");
        } finally {
            Configuration.logger.traceExit();
            stop();
        }
    }

    public void stop() throws ServerException {
        Configuration.logger.traceEntry();

        try {
            serverSocket.close();
        } catch (IOException e) {
            throw new ServerException("Error closing the server");
        }

        Configuration.logger.traceExit();
    }

    protected abstract void beginConversation(Socket connection);

}
